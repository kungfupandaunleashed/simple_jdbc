package org.example;

import java.sql.*;
import java.util.ArrayList;

public class App
{
    public static String toPascalCase(String str) {
        String res;
        if(str.length() < 1) {
            return str;
        }
        res = String.format("%c",str.charAt(0)).toUpperCase();
        if(str.length() > 1) {
            res+=str.substring(1).toLowerCase();
        }

        return res;
    }

    public static void main( String[] args ) {
        String user = "root";
        String password = "Nishanth@2002123456789Arun64";
        Connection conn;

        String rollNumber = "CS20B025";
        String Name = "Nishanth";
        String State = "Tamilnadu";

        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbms_schema",
                    user,password);
            System.out.println("Connection established successfully");
        } catch (SQLException e) {
            System.out.println("Unable to find mysql driver in class path");
            throw new RuntimeException(e);
        }

        String createTable = String.format("CREATE TABLE %s_%s (" +
                "Name VARCHAR(100) NOT NULL," +
                "RollNo CHAR(15) NOT NULL," +
                "State VARCHAR(50));",rollNumber.toUpperCase(),toPascalCase(Name));

        ArrayList<String> uselessInsert = new ArrayList<String>();
        int count = 3;

        for(int i=0;i<count;i++) {
            uselessInsert.add(String.format("INSERT INTO %1$s_%2$s VALUES ('%2$s - %4$d','%1$s','%3$s');",rollNumber.toUpperCase(),toPascalCase(Name),
                    toPascalCase(State),i));
        }
        String queryAll = String.format("SELECT * FROM %s_%s;",rollNumber.toUpperCase(),toPascalCase(Name));

        Statement statement;
        try {
            statement = conn.createStatement();
            statement.execute(createTable);
            System.out.println("New table created successfully");
        } catch (SQLException e) {
            System.out.println("Unable to create table");
            throw new RuntimeException(e);
        }
        ResultSet rs;

        try {

            for(String command: uselessInsert) {
                statement.execute(command);
            }

            System.out.println("Sample commands run successfully");

            rs = statement.executeQuery(queryAll);
            System.out.println("Query all command run successfully");

            int index = 1;
            while(rs.next()) {
                System.out.println(index++ + ") Name    :"+rs.getString(1)+
                        "\n   "+"RollNo  :"+rs.getString(2)+
                        "\n   "+"State   :"+rs.getString(3)+"\n");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
